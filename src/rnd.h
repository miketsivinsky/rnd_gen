#if !defined(RND_H)
#define RND_H

#include <ctype.h>
#include <ctime>
#include <random>

//------------------------------------------------------------------------------
template <typename T> class rnd_gen
{
    public:
        rnd_gen(T min_val, T max_val) : gen_m(time(0)), uid_m(min_val, max_val)  {}
        T gen() { return uid_m(gen_m); }
        
    protected:
        std::mt19937                     gen_m;
        std::uniform_int_distribution<T> uid_m;
};

//------------------------------------------------------------------------------
class base_gen
{
    public:
        virtual ~base_gen() {}
        virtual unsigned gen() { return gen_m.gen(); }
        virtual void run(unsigned pass_num);
        virtual void print_stat() {  print_bucket(bucket_m); }
        
    protected:
        base_gen(unsigned range, unsigned range2) :
                                                   gen_m(0, range-1),
                                                   pass_num_m(0),
                                                   bucket_m(range2, 0)
                                                   {}
        void print_bucket(const std::vector<unsigned>& bucket);
    
        rnd_gen<unsigned>     gen_m;
        unsigned              pass_num_m;
        std::vector<unsigned> bucket_m;
};

//------------------------------------------------------------------------------
// Reference gen
//------------------------------------------------------------------------------
class ref_gen : public base_gen
{
    public:
        ref_gen(unsigned range) : base_gen(range, range) {}
};

//------------------------------------------------------------------------------
// Dima gen - good for partial range_in, range_out pair (5,7), but wrong in general case
//------------------------------------------------------------------------------
class D_gen : public base_gen
{
    public:
        static unsigned bucket_sum_range(unsigned range_in, unsigned range_out) { return (range_in - 1)*range_out + 1; }
        D_gen(unsigned range_in, unsigned range_out) :
                                                             base_gen(range_in, range_out),
                                                             bucket_sum_m(bucket_sum_range(range_in, range_out), 0)
                                                             {}
        virtual unsigned gen() override;
        void run2(unsigned pass_num);
        void print_stat2()
        {
            print_bucket(bucket_sum_m);
            print_bucket(bucket_m);
        }
        
    protected:
        std::vector<unsigned> bucket_sum_m;
};

//------------------------------------------------------------------------------
// Julia gen - proper solution, probably can be optimized
//------------------------------------------------------------------------------
class J_gen : public base_gen
{
    public:
        J_gen(unsigned range_in, unsigned range_out);
        virtual unsigned gen() override;

    protected:
        unsigned              pow_m;
        unsigned              max_m;
        unsigned              range_in_m;
        unsigned              range_out_m;
};

#endif // RND_H
