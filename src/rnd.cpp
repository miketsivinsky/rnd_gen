#include <cstdio>
#include "rnd.h"


using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void base_gen::print_bucket(const std::vector<unsigned>& bucket)
{
    double sum = 0.0;

    for(unsigned i = 0; i < bucket.size(); i++) {
        double res = 1.0*bucket[i]/pass_num_m;
        sum += res;
        printf("[%2d] %8.6f\n",i,res);
    }
    printf("\nsum: %8.6f\n\n",sum);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void base_gen::run(unsigned pass_num)
{
    bucket_m.assign(bucket_m.size(), 0);
    pass_num_m = pass_num;
    for(unsigned i = 0; i < pass_num; ++i) {
        unsigned bucket_idx = gen();
        ++bucket_m[bucket_idx];
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
unsigned D_gen::gen()
{
    unsigned res = 0;
    size_t range_out = bucket_m.size();
    for(unsigned i = 0; i < range_out; i++) {
        res += gen_m.gen();
    }
    res = res % range_out;
    return res;
}

//------------------------------------------------------------------------------
void D_gen::run2(unsigned pass_num)
{
    bucket_sum_m.assign(bucket_sum_m.size(), 0);
    bucket_m.assign(bucket_m.size(), 0);
    
    pass_num_m = pass_num;
    size_t range_sum = bucket_sum_m.size();
    size_t range_out = bucket_m.size();

    //---
    for(unsigned i = 0; i < pass_num; ++i) {
        unsigned res = 0;
        for(unsigned j = 0; j < range_out; j++) {
            res += gen_m.gen();
        }
        ++bucket_sum_m[res];
    }

    //---
    for(unsigned i = 0; i < range_sum; i++) {
        unsigned idx = (i % range_out);
        bucket_m[idx] = bucket_m[idx] + bucket_sum_m[i];
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
J_gen::J_gen(unsigned range_in, unsigned range_out) :
                                                       base_gen(range_in, range_out),
                                                       range_in_m(range_in),
                                                       range_out_m(range_out)
{
    pow_m = 1;
    unsigned max_in = range_in;
    while(max_in < range_out) {
            max_in *= range_in;
            ++pow_m;
    }

    max_m = (max_in/range_out)*range_out - 1;
    printf("\n[D] max_m: %3d, pow_m: %3d\n\n", max_m, pow_m);
}

//------------------------------------------------------------------------------
unsigned J_gen::gen()
{
    unsigned extra_iter = 0;
    unsigned val;
    while(1) {
        val = 0;
        unsigned deg = 1;
        for(unsigned p = 1; p <= pow_m; p++) {
            unsigned rnd_in = gen_m.gen(); 
            //printf("rnd_in %3d\n",rnd_in);
            val = val + deg*rnd_in;
            deg *= range_in_m;
        }
        if(val <= max_m) {
            break;
        }
        ++extra_iter;
    }
    val = (val % range_out_m);
    //printf("val: %3d, extra_iter: %3d\n",val, extra_iter);
    return val;
}
