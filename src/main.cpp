#include <cstdio>
#include "rnd.h"

using namespace std;


//------------------------------------------------------------------------------
struct RangePair_t {
    const unsigned W1;
    const unsigned W2;
};

RangePair_t P1 = {  5,  7 }; // good case for D_gen
RangePair_t P2 = {  2,  9 }; // bad case for D_gen
RangePair_t P3 = {  3, 11 }; // bad case for D_gen
RangePair_t P4 = {  2, 15 }; // bad case for D_gen

//------------------------------------------------------------------------------
const unsigned N = 10000000;
RangePair_t    P = P1;

#define ENA_REF_GEN
#define ENA_D_GEN   1
#define ENA_J_GEN

//------------------------------------------------------------------------------
int main()
{
    printf("*** rnd gen transform test (range_in to range_out) ***\n");
    printf("range_in:  %2d\n",P.W1);
    printf("range_out: %2d\n",P.W2);
    
    //--- reference gen
    #if defined(ENA_REF_GEN)
        printf("\n --- ref gen buckets ---\n");
        ref_gen ref_gen_inst(P.W2);
        ref_gen_inst.run(N);
        ref_gen_inst.print_stat();
    #endif

    //--- Dima gen
    #if defined(ENA_D_GEN)
        D_gen d_gen(P.W1,P.W2);
        #if(ENA_D_GEN == 1)
            printf("\n --- D_gen buckets (standard out) ---\n");
            d_gen.run(N);
            d_gen.print_stat();
        #else
            printf("\n --- D_gen buckets (extended out) ---\n");
            d_gen.run2(N);
            d_gen.print_stat2();
        #endif
    #endif

    //--- Julia gen
    #if defined(ENA_J_GEN)
        printf("\n --- J_gen buckets ---\n");
        J_gen j_gen(P.W1,P.W2);
        j_gen.run(N);
        j_gen.print_stat();
    #endif

    return 0;
}
